module segment7(
	output[0:6] seg,
	input[1:0] c
);
	wire sig1;
	
	// Using assign statements
	assign sig1 = c[1] | ~c[0];
	
	// Connect segment
	assign seg[0] = sig1;
	assign seg[1] = c[0];
	assign seg[2] = c[0];
	assign seg[3] = c[1];
	assign seg[4] = c[1];
	assign seg[5] = sig1;
	assign seg[6] = c[1];
	
endmodule
