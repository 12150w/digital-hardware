module part3_truth_table(
	input A, B, C, D,
	output reg Q
);
	
	always @(*) begin
		case({A, B, C, D})
			4'b0000: Q = 1;
			4'b0001: Q = 0;
			4'b0010: Q = 1;
			4'b0011: Q = 0;
			4'b0100: Q = 1;
			4'b0101: Q = 1;
			4'b0110: Q = 1;
			4'b0111: Q = 1;
			4'b1000: Q = 0;
			4'b1001: Q = 0;
			4'b1010: Q = 1;
			4'b1011: Q = 0;
			4'b1100: Q = 0;
			4'b1101: Q = 1;
			4'b1110: Q = 1;
			4'b1111: Q = 1;
		endcase
	end
	
endmodule
