module part3_test;
	reg[3:0] moduleIn;
	wire moduleOut;
	
	// Device to test
	part3_truth_table device(
		.A(moduleIn[3]),
		.B(moduleIn[2]),
		.C(moduleIn[1]),
		.D(moduleIn[0]),
		
		.Q(moduleOut)
	);
	
	initial begin
		//$dumpfile(`SIG_OUT);
		//$dumpvars(0, moduleIn);
		//$dumpvars(1, moduleOut);
		
		// Initial signals
		moduleIn = 0;
		
		// Wait for all input cases
		#16
		
		$finish;
	end
	
	// Generate input
	always begin
		#1
		moduleIn = moduleIn + 1;
	end
	
endmodule
