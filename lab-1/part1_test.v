module part1_test;
	reg[1:0] control;
	wire[6:0] hex;
	
	// Test Procedure
	initial begin
		//$dumpfile(`SIG_OUT);
		//$dumpvars(0, control);
		//$dumpvars(1, hex);
		
		// Check "d"
		control = 2'b00;
		#20 if(hex != 7'b1000010) $error("Error displaying 'd'");
		
		// Check "E"
		control = 2'b01;
		#20 if(hex != 7'b0110000) $error("Error displaying 'E'");
		
		// Check "1"
		control = 2'b10;
		#20 if(hex != 7'b1001111) $error("Error displaying '1'");
		
		// Check blank
		control = 2'b11;
		#20 if(hex != 7'b1111111) $error("Error displaying blank");
		
		$finish;
	end
	
	// Hook up the device
	part1 device(
		.SW(control),
		.HEX0(hex)
	);
	
endmodule
