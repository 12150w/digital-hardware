/*
 * Demo Hello World
 *
 * You know what this does.
 */
#include <stdio.h>

int main()
{
  printf("Hello from Nios II!\n");

  return 0;
}
