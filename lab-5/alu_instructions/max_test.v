module max_test;

reg [31:0] dataa, datab;

wire [31:0] result;

max max1(dataa, datab, result);

	initial begin

	dataa = 32'd0;
	datab = 32'd0;
	
	#100 if(result != 32'd0) $error("0 > 0 was not 0");
	
	dataa = 32'd0;
	datab = -32'd1;
	
	#100 if(result != 32'd0) $error("Result should be A");
	
	dataa = 32'd0;
	datab = 32'd1;
	
	#100 if(result != 32'd1) $error("Result should be B");
	
	dataa = 32'd0;
	datab = 32'd2;
	
	#100 if(result != 32'd2) $error("Result should be B");
	
	dataa = 32'd0;
	datab = 32'd3;
	
	#100 if(result != 32'd3) $error("Result should be B");
	
	dataa = 32'd1;
	datab = 32'd0;
	
	#100 if(result != 32'd1) $error("Result should be A");
	
	dataa = 32'd2;
	datab = 32'd0;
	
	#100 if(result != 32'd2) $error("Result should be A");
	
	dataa = 32'd3;
	datab = 32'd0;
	
	#100 if(result != 32'd3) $error("Result should be A");
	

	

	end

endmodule
