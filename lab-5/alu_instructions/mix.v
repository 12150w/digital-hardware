/*
	Mix Command
	
	This mixes (interlaces) the inputs.
	This is achieved by shifting the inputs and masking them.
*/
module mix(
	input[31:0] dataa, datab,
	output[31:0] result
);
	
	// Mask the inputs
	wire[31:0] aMasked, bMasked;
	assign aMasked = dataa & 32'h55555555;
	assign bMasked = (datab<<1) & 32'h55555555;
	
	assign result = aMasked | bMasked;
	
endmodule
