module max(
	input signed[31:0] dataa, datab,
	output signed[31:0] result);
	
	assign result = dataa > datab ? dataa : datab;
	
	endmodule
	
	