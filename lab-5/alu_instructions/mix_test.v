/*
	Mix Command Test
	
	Tests the mix module.
*/
module mix_test;
	reg[31:0] dataa, datab;
	wire[31:0] result;
	
	mix dut(dataa, datab, result);
	
	initial begin
		dataa = 32'd0;
		datab = 32'd0;
		
		// Mix all 0 with all 1
		dataa = 32'hFFFFFFFF;
		#10 if(result != 32'h55555555) $error("Invalid: MIX(%h, %h) is not %h", dataa, datab, result);
		
		// Mix general case
		dataa = 32'h0F0F0F0F;
		#10 if(result != 32'h05050505) $error("Invalid: MIX(%h, %h) is not %h", dataa, datab, result);
		
	end
	
endmodule
