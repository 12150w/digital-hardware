#include "system.h"
#include "altera_avalon_performance_counter.h"
#include "alu_c.h"
#include <stdio.h>

int a = 5, b = 70, res;

int main() {
	printf("Starting timing\n\n");

	// Max performance
	PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
	res = ALT_CI_ALU_MAX_0(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 1);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 2);
	res = getMax(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 2);

	printf( "Max ALU: %lld\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,1) );
	printf( "Max C: %lld\n\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,2) );

	// Mix performance
	PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
	res = ALT_CI_ALU_MIX_0(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 1);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 2);
	res = getMix(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 2);

	printf( "Mix ALU: %lld\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,1) );
	printf( "Mix C: %lld\n\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,2) );

	// Palindrome performance
	PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
	//res = ALT_CI_PALINDROME_0(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 1);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 2);
	res = isPalindrome(a);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 2);

	printf( "Palindrome ALU: %lld\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,1) );
	printf( "Palindrome C: %lld\n\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,2) );


	return 0;
}
