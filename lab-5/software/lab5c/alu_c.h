#ifndef _ALU_C_
#define _ALU_C_

int getMax(int a, int b);
int getMix(int a, int b);
int getPopcount(int a);
int isPalindrome(int a);

#endif
