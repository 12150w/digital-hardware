#include "alu_c.h"

// getMax returns the highest number, either a or b
int getMax(int a, int b) {
	if(a >= b) return a;
	else return b;
}

// getMix returns the mix of a and b
int getMix(int a, int b) {
	return (a & 0x55555555) | ((b<<1) & 0x55555555);
}

// isPalindrome returns non-zero if a is a palindrome
//TODO: implement this
int isPalindrome(int a) {
	int test = 0;
	int i = a;
	while (1) {
		if(i & 0x1) {
			test++;
		}
		i = i >> 1;
		if (i) {
			test = test << 1;
		} else {
			break;
		}
	}
	return test;
}

// getPopcount returns the number of 1's in a
int getPopcount(int a) {
	int bitCount = sizeof(a)*8;
	int oneCount = 0;
	int i;

	for(i=0; i < bitCount; i++) {
		if(a & 1) {
			oneCount++;
		}
		a <<= 1;
	}

	return oneCount;
}
