#include "system.h"
#include "altera_avalon_performance_counter.h"
#include <stdio.h>

int a = 5, b = 70, max;

int main() {
	printf("Starting timing\n");

	PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
	max = ALT_CI_ALU_MAX_0(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 1);


	printf( "Max ALU: %lld\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,1) );
	return 0;
}
