/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */
#include "system.h"
#include <stdio.h>

int a = 3;
int b = 75;
int max;

int main()
{
	/*PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);

	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
	max = ALT_CI_ALU_MAX_0(a, b);
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 1);
*/
	printf( "Max ALU : %lld\n", perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE,1) );

	return 0;
}
