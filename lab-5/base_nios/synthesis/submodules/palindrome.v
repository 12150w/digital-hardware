module palendrome(
	input signed[31:0] dataa,
	output signed[31:0] result);
	
	wire [15:0] xnors; 
	
	assign xnors[0] = dataa[31] ~^ dataa[0];
	assign xnors[1] = dataa[30] ~^ dataa[1];
	assign xnors[2] = dataa[29] ~^ dataa[2];
	assign xnors[3] = dataa[28] ~^ dataa[3];
	assign xnors[4] = dataa[27] ~^ dataa[4];
	assign xnors[5] = dataa[26] ~^ dataa[5];
	assign xnors[6] = dataa[25] ~^ dataa[6];
	assign xnors[7] = dataa[24] ~^ dataa[7];
	assign xnors[8] = dataa[23] ~^ dataa[8];
	assign xnors[9] = dataa[22] ~^ dataa[9];
	assign xnors[10] = dataa[21] ~^ dataa[10];
	assign xnors[11] = dataa[20] ~^ dataa[11];
	assign xnors[12] = dataa[19] ~^ dataa[12];
	assign xnors[13] = dataa[18] ~^ dataa[13];
	assign xnors[14] = dataa[17] ~^ dataa[14];
	assign xnors[15] = dataa[16] ~^ dataa[15];
	
	assign result = xnors == 16'hFFFF ? 32'd1 : 32'd0;
	
	endmodule
	
	