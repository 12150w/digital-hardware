/*
	ALU Module Test
		Verifies the operation of the ALU module
*/

module alu_test();
	reg[31:0] A, B;
	wire[31:0] Out;
	reg[2:0] Function;
	wire Zflag;
	
	initial begin
		
		// Adding
		Function = 3'b010;
		
		$display("Testing: 0 + 0");
		A = 0; B = 0;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0 + (-1)");
		A = 0; B = -1;
		#10 if(Out != 32'hFFFFFFFF) $error("Falure: Out = %h", Out);
		
		$display("Testing: 1 + (-1)");
		A = 1; B = -1;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: 1 + 0xFF");
		A = 1; B = 32'h000000FF;
		#10 if(Out != 32'h00000100) $error("Falure: Out = %h", Out);
		
		// Subtraction
		Function = 3'b110;
		
		$display("Testing: 0 - 0");
		A = 0; B = 0;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0 - (-1)");
		A = 0; B = -1;
		#10 if(Out != 32'h00000001) $error("Falure: Out = %h", Out);
		
		$display("Testing: 1 - 1");
		A = 1; B = 1;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0x100 - 1");
		A = 32'h00000100; B = 1;
		#10 if(Out != 32'h000000FF) $error("Falure: Out = %h", Out);
		
		// SLT (A less than B test)
		Function = 3'b111;
		
		$display("Testing: 0 < 0");
		A = 0; B = 0;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0 < 1");
		A = 0; B = 1;
		#10 if(Out != 32'h00000001) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0 < -1");
		A = 0; B = -1;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: 1 < 0");
		A = 1; B = 0;
		#10 if(Out != 32'h00000000) $error("Falure: Out = %h", Out);
		
		$display("Testing: -1 < 0");
		A = -1; B = 0;
		#10 if(Out != 32'h00000001) $error("Falure: Out = %h", Out);
		
		// ANDing
		Function = 3'b000;
		
		$display("Testing: 0xFFFFFFFF & 0xFFFFFFFF");
		A = 32'hFFFFFFFF; B = 32'hFFFFFFFF;
		#10 if(Out != 32'hFFFFFFFF) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0xFFFFFFFF & 0x12345678");
		A = 32'hFFFFFFFF; B = 32'h12345678;
		#10 if(Out != 32'h12345678) $error("Falure: Out = %h", Out);
		
		A = 32'h12345678; B = 32'h87654321;
		$display("Testing: 0x12345678 & 0x87654321 = 0x%h", (A&B));
		#10 if(Out != (A&B)) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0x00000000 & 0xFFFFFFFF");
		A = 0; B = 32'hFFFFFFFF;
		#10 if(Out != 0) $error("Falure: Out = %h", Out);
		
		// ORing
		Function = 3'b001;
		
		$display("Testing: 0xFFFFFFFF | 0xFFFFFFFF");
		A = 32'hFFFFFFFF; B = 32'hFFFFFFFF;
		#10 if(Out != 32'hFFFFFFFF) $error("Falure: Out = %h", Out);
		
		A = 32'h12345678; B = 32'h87654321;
		$display("Testing: 0x12345678 | 0x87654321 = 0x%h", (A|B));
		#10 if(Out != (A|B)) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0x00000000 | 0xFFFFFFFF");
		A = 0; B = 32'hFFFFFFFF;
		#10 if(Out != 32'hFFFFFFFF) $error("Falure: Out = %h", Out);
		
		$display("Testing: 0x00000000 | 0x00000000");
		A = 0; B = 0;
		#10 if(Out != 0) $error("Falure: Out = %h", Out);
		
		$finish;
	end
	
	// Test that zflag is setup (does not account for delays)
	always @(Out) begin
		if(Out == 0 && Zflag != 1) $error("Zero flag set violation");
		else if(Out != 0 && Zflag != 0) $error("Zero flag clear violation");
	end
	
	alu dev(
		.A(A), .B(B), .F(Function),
		.Out(Out), .Zero(Zflag)
	);
	
endmodule
