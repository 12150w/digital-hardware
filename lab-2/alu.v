/*
	ALU Module
		The description for this ALU module is on the first page of the lab
*/

module alu(
	input[2:0] F,
	input wire signed[31:0] A, B,
	output reg signed[31:0] Out,
	output wire Zero
);
	
	// Handle the zero flag (using combinational logic)
	assign Zero = Out == 0;
	
	// Handle function selection
	always @(F, A, B) case(F)
		3'b000: Out <= A & B;
		3'b001: Out <= A | B;
		3'b010: Out <= A + B;
		// 3'b011 not used
		3'b100: Out <= A & ~B;
		3'b101: Out <= A | ~B;
		3'b110: Out <= A - B;
		3'b111: Out <= A < B;
		
		default: Out <= 0;
	endcase
	
endmodule
