/*
	Part 3 (Top Level)
*/

module part_3(
	input wire[9:0] SW,
	input wire[1:0] KEY,
	output wire[6:0] HEX5, HEX4, HEX3, HEX2, HEX1, HEX0
);
	
	// Input flip flops
	//   SW[9] A enable, SW[8] B enable
	//   ActiveInput is connected to the activated input (used for display)
	wire[7:0] A, B;
	reg[7:0] ActiveInput;
	flip_flop #(7) FF_A(
		.D(SW[7:0]), .Q(A), .Clock(KEY[1]), .Enable(SW[9]), .Reset(KEY[0])
	);
	flip_flop #(7) FF_B(
		.D(SW[7:0]), .Q(B), .Clock(KEY[1]), .Enable(SW[8]), .Reset(KEY[0])
	);
	always @(SW[9:8]) case(SW[9:8])
		2'b10: ActiveInput <= A; // A selected
		2'b01: ActiveInput <= B; // B selected
		default: ActiveInput <= 8'h00; // None selected (turn off display)
	endcase
	
	// Multiplier
	//   P is after the flip flop, Pout is before
	wire[15:0] Pout, P;
	mult2 #(8) Multiplier(
		.A(A), .B(B), .P(Pout)
	);
	flip_flop #(15) FF_P(
		.D(Pout), .Q(P), .Clock(KEY[1]), .Enable(1), .Reset(1)
	);
	
	// Display
	//   HEX5, HEX4 display active input (A or B, or A and B if both enabled)
	//   HEX3, HEX2, HEX1, HEX0 display the product
	hex_7seg InputDecoderH(.display_on(1), .hex_digit(ActiveInput[7:4]), .seg(HEX5));
	hex_7seg InputDecoderL(.display_on(1), .hex_digit(ActiveInput[3:0]), .seg(HEX4));
	
	hex_7seg InputDecoderHH(.display_on(1), .hex_digit(P[15:12]), .seg(HEX3));
	hex_7seg InputDecoderHL(.display_on(1), .hex_digit(P[11:8]), .seg(HEX2));
	hex_7seg InputDecoderLH(.display_on(1), .hex_digit(P[7:4]), .seg(HEX1));
	hex_7seg InputDecoderLL(.display_on(1), .hex_digit(P[3:0]), .seg(HEX0));
	
endmodule
