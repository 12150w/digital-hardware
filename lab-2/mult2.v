/*
	N Bit Multiplier (part 3)
*/

module mult2 #(parameter N = 2) (
	input[N - 1:0] A, B,
	output[N*2 - 1:0] P
);
	
	// sums contains the sum for each layer
	wire[N - 1:0] sums[N - 1:0];
	
	// carries contains the carry signal from each layer
	wire[N - 1:0] carries;
	
	// Generate each layer
	genvar k;
	generate
		for(k=0; k < N; k = k + 1) begin : layers
			
			// ands contains the and gates for this layer
			wire[N - 1:0] ands;
			genvar i;
			for(i=0; i < N; i = i + 1) begin : input_ands
				assign ands[i] = A[i] & B[k];
			end
			
			// if this is the first layer, no adder is needed
			if(k == 0) begin
				assign carries[k] = 0;
				assign sums[k] = ands;
			end
			
			// if this is NOT the first layer, make the adders
			else begin
				adder #(N) add(
					.A(ands),
					.B({carries[k - 1], sums[k-1][N - 1:1]}),
					.Cin(1'b0),
					.Out(sums[k]),
					.Cout(carries[k])
				);
			end
			
			// Carry the lowest bit to the output
			assign P[k] = sums[k][0];
			
		end
	endgenerate
	
	// Assign the high bits of P
	assign P[N*2 - 1:N] = {carries[N - 1], sums[N - 1][N - 1:1]};
	
endmodule
