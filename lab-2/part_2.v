/*
	Part 2 (part_2)
		Implements a multiplier for part 2 of the lab
*/

module part_2(
	input wire[7:0] SW,
	output wire[6:0] HEX0, HEX2, HEX5, HEX4
);
	
	wire[7:0] P;
	
	mult1 multiplier(
		.A(SW[7:4]),
		.B(SW[3:0]),
		.P(P)
	);
	
	// Display inputs
	hex_7seg seg0(
		.hex_digit(SW[3:0]),
		.seg(HEX0),
		.display_on(1)
	);
	hex_7seg seg1(
		.hex_digit(SW[7:4]),
		.seg(HEX2),
		.display_on(1)
	);
	
	// Display product
	hex_7seg seg2(
		.hex_digit(P[7:4]),
		.seg(HEX5),
		.display_on(1)
	);
	hex_7seg seg3(
		.hex_digit(P[3:0]),
		.seg(HEX4),
		.display_on(1)
	);
	
endmodule
