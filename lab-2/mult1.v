/*
	4-Bit Multiplier (mult1)
		Implemented using design in part 2
*/

module mult1(
	input wire[3:0] A, B,
	output wire[7:0] P
);
	
	// AND gates
	wire[3:0] b0And, b1And, b2And, b3And;
	assign b0And = {B[0]&A[3], B[0]&A[2], B[0]&A[1], B[0]&A[0]};
	assign b1And = {B[1]&A[3], B[1]&A[2], B[1]&A[1], B[1]&A[0]};
	assign b2And = {B[2]&A[3], B[2]&A[2], B[2]&A[1], B[2]&A[0]};
	assign b3And = {B[3]&A[3], B[3]&A[2], B[3]&A[1], B[3]&A[0]};
	
	// Adder Sums and Carries
	wire[3:0] addSum0, addSum1, addSum2, addC0, addC1, addC2;
	
	// Bit 0
	assign P[0] = b0And[0];
	
	// Bit 1
	fa a00(0       , b0And[1], b1And[0], addSum0[0], addC0[0]);
	fa a01(addC0[0], b0And[2], b1And[1], addSum0[1], addC0[1]);
	fa a02(addC0[1], b0And[3], b1And[2], addSum0[2], addC0[2]);
	fa a03(addC0[2], 0       , b1And[3], addSum0[3], addC0[3]);
	assign P[1] = addSum0[0];
	
	// Bit 2
	fa a10(0       , b2And[0], addSum0[1], addSum1[0], addC1[0]);
	fa a11(addC1[0], b2And[1], addSum0[2], addSum1[1], addC1[1]);
	fa a12(addC1[1], b2And[2], addSum0[3], addSum1[2], addC1[2]);
	fa a13(addC1[2], b2And[3], addC0[3]  , addSum1[3], addC1[3]);
	assign P[2] = addSum1[0];
	
	// Final layer
	fa a20(0       , b3And[0], addSum1[1], addSum2[0], addC2[0]);
	fa a21(addC2[0], b3And[1], addSum1[2], addSum2[1], addC2[1]);
	fa a22(addC2[1], b3And[2], addSum1[3], addSum2[2], addC2[2]);
	fa a23(addC2[2], b3And[3], addC1[3]  , addSum2[3], addC2[3]);
	assign {P[6], P[5], P[4], P[3]} = addSum2;
	assign P[7] = addC2[3];
	
endmodule
