/*
	mult2 Test
	
	Tests the mult2 module
*/

module mult2_test;
	reg[7:0] A, B;
	wire[15:0] P;
	reg[15:0] predicted;
	
	// Instantiate the multiplier
	mult2 #(8) device(
		.A(A), .B(B), .P(P)
	);
	
	initial begin
		A = 2'h00;
		B = 2'h00;
		
		repeat(256) begin
			repeat(256) begin
				#1
				predicted = A*B;
				if(P != predicted) $error("%d * %d is not 0x%h, should be 0x%h", A, B, P, predicted);
				
				B = B + 1'b1;
			end
			
			A = A + 1'b1;
		end
		
		$finish;
	end
	
endmodule
