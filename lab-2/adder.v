/*
	Variable Width Adder
	
	A simple variable width ripple carry adder
*/

module adder #(parameter WIDTH = 2) (
	input wire[WIDTH - 1:0] A, B,
	input wire Cin,
	output wire[WIDTH - 1:0] Out,
	output wire Cout
);
	
	// carries are the carry signals from each full adder
	wire carries[WIDTH - 1:0];
	assign Cout = carries[WIDTH - 1];
	
	// Create adders
	genvar i;
	generate
		for(i = 0; i < WIDTH; i = i + 1) begin : adders
			
			// faCin is the carry in for this specific full adder
			wire faCin;
			if(i == 0) assign faCin = Cin;
			else assign faCin = carries[i - 1];
			
			fa add(
				.Cin(faCin),
				.A(A[i]),
				.B(B[i]),
				.Sum(Out[i]),
				.Cout(carries[i])
			);
		end
	endgenerate
	
endmodule
