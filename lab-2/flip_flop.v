/*
	D-Q Flip Flop
	
	Describes a simple D-Q flip flop.
	
	Enable: 1 sets Q to D at clock rising edge, 0 persists current Q (active high)
	Reset: synchronous reset, resets when 0 (active low)
*/

module flip_flop(
	D, Q,
	Clock, Enable, Reset
);
	parameter WIDTH = 0;
	
	// Define data signals
	input wire[WIDTH:0] D;
	output reg[WIDTH:0] Q;
	input wire Clock, Enable, Reset;
	
	always @(posedge Clock) begin
		
		// If reset
		if(Reset == 0)
			Q <= 0;
		
		// If disabled
		else if(Enable == 0)
			Q <= Q;
		
		// Otherwise enabled
		else
			Q <= D;
		
	end
	
endmodule
