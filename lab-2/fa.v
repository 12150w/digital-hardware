/*
	Full Adder Module (fa)
		Implements single bit basic full adder
*/

module fa(
	input wire Cin, A, B,
	output wire Sum, Cout
);
	
	// Describe logic with expressions
	assign Sum = A ^ B ^ Cin;
	assign Cout = (A&B) | (Cin & (A^B));
	
endmodule
