/*
	Part 2 Test
	
	Test multiplication using the mult1 module
*/

module part_2_test;
	reg[3:0] A, B;
	wire[7:0] P;
	
	// Instantiate the device
	mult1 device(
		.A(A), .B(B), .P(P)
	);
	
	initial begin
		A = 0;
		B = 0;
		
		repeat(16) begin
			repeat(16) begin
				#10
				if(P != A*B) $error("Multiplication error %d * %d != %d", A, B, P);
				
				B = B + 1;
			end
			
			A = A + 1;
		end
		
		$finish;
	end
	
endmodule
