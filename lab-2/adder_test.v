/*
	Adder Test
	
	Tests the adder module
*/

module adder_test;
	reg[7:0] a, b;
	reg cin;
	wire[7:0] sum;
	wire cout;
	
	adder #(8) Device(
		.A(a),
		.B(b),
		.Cin(cin),
		.Out(sum),
		.Cout(cout)
	);
	
	initial begin
		
		// Test carry in
		cin = 1'b1;
		a = 8'h0F;
		b = 8'h00;
		#10 if(sum != 8'h10) $error("Carry in was not used in sum");
		
		// Test random sum
		cin = 1'b0;
		a = 8'd17;
		b = 8'd55;
		#10 if(sum != 8'd72) $error("Sum was not calculated properly");
		
		// Test carry out
		cin = 1'b1;
		a = 8'hFF;
		b = 8'h00;
		#10 if(cout != 1'b1) $error("Carry out was not calculated");
		
		$finish;
	end
	
endmodule
