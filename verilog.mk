#
# Shared Verilog Makefile
#   SOURCES: list of verilog sources that describe your device
#   TOP: name of the top level entity
#   PROJ: project name
#   Q_FAMILY: product family name
#   Q_DEVICE: product name
#   Q_MAPFILE: pin mapping file
#   [TEST]: list of verilog sources that test your device (test benches)
#
#   [Q_DIR]: directory of the quartus installation binaries (end in "/")
#   [MS_DIR]: directory of the model-sim altera binaries (end in "/")
#
SELF_DIR=$(dir $(lastword $(MAKEFILE_LIST)))
-include $(SELF_DIR)user.mk

ifndef TEST
	TEST =
endif
ifndef Q_DIR
	Q_DIR =
endif
ifndef MS_DIR
	MS_DIR =
endif


.PHONY: clean icarus icarus-compile gtkwave q-map q-fit q-asm q-sta q-rtl q-pgm ms-sim ms-time $(TEST)

#
# Utility
#   make-output: Verifies the output directory
#
OUT=out/
IN_OUT=cd $(OUT) && 
OUTDIR=$(OUT).blank
$(OUTDIR):
	@echo "Making Output Directory"
	@mkdir -p $(OUT)
	@echo "" > $(OUTDIR)
clean:
	rm -rf $(OUT)


#
# Quartus (the big bad one)
#   q-map: runs the mapper
#   q-fit: runs the fitter
#   q-asm: runs the assembler
#   q-sta: runs the timing analyzer
#   q-rtl: opens the rtl logic viewer
#   q-pgm: programs the board
#
Q_SH=$(Q_DIR)quartus_sh
Q_PROJFILES=$(OUT)$(PROJ).qpf $(OUT)$(PROJ).qsf
Q_STAMP=echo done > 
ifndef Q_MAPFILE
	Q_MAPFILE=$(OUTDIR)
endif
ifndef Q_PGM_CABLE
	Q_PGM_CABLE=DE-SoC [1-1.2]
endif
ifndef Q_PGM_DEVICE
	Q_PGM_DEVICE=1
endif
Q_PGM_FLAGS=--cable='$(Q_PGM_CABLE)' --mode=jtag

# Interface
q-map: $(Q_PROJFILES) $(OUT)$(PROJ).map.rpt
q-fit: $(Q_PROJFILES) $(OUT)$(PROJ).fit.rpt
q-asm: $(Q_PROJFILES) $(OUT)$(PROJ).asm.rpt
q-sta: $(Q_PROJFILES) $(OUT)$(PROJ).sta.rpt
q-rtl: $(Q_PROJFILES) $(OUT)$(PROJ).map.rpt
	$(IN_OUT) \
	$(Q_DIR)qnui $(PROJ)
q-pgm: $(Q_PROJFILES) $(OUT)$(PROJ).asm.rpt
	$(IN_OUT) \
	$(Q_DIR)quartus_pgm $(Q_PGM_FLAGS) -o "P;$(PROJ).sof@$(Q_PGM_DEVICE)"

# Creates Project
$(Q_PROJFILES): $(OUTDIR)
	$(IN_OUT) \
	$(Q_SH) --prepare -f "$(Q_FAMILY)" -t $(TOP) $(PROJ)
	cat $(Q_MAPFILE) >> $(OUT)$(PROJ).qsf

# Maps Sources (Analysis & Synthesis)
Q_MAP_ARGS=--read_settings_files=on --part=$(Q_DEVICE) $(addprefix --source=../,$(SOURCES))
$(OUT)$(PROJ).map.rpt: $(OUT)map.chg $(SOURCES)
	$(IN_OUT) \
	$(Q_DIR)quartus_map $(Q_MAP_ARGS) $(PROJ)

# Fits map (Fit & Place)
Q_FIT_ARGS=--part=$(Q_DEVICE) --read_settings_files=on
$(OUT)$(PROJ).fit.rpt: $(OUT)fit.chg $(OUT)$(PROJ).map.rpt
	$(IN_OUT) \
	$(Q_DIR)quartus_fit $(Q_FIT_ARGS) $(PROJ)
	$(Q_STAMP) $(OUT)asm.chg
	$(Q_STAMP) $(OUT)sta.chg

# Assembles fit (Assembler)
$(OUT)$(PROJ).asm.rpt: $(OUT)asm.chg $(OUT)$(PROJ).fit.rpt
	$(IN_OUT) \
	$(Q_DIR)quartus_asm $(PROJ)

# Timing analysis (before simulation)
$(OUT)$(PROJ).sta.rpt: $(OUT)sta.chg $(OUT)$(PROJ).fit.rpt
	$(IN_OUT) \
	$(Q_DIR)quartus_sta $(PROJ)

# Smart Report
$(OUT)smart.log: $(Q_PROJFILES)
	$(IN_OUT) \
	$(Q_SH) --determine_smart_action $(PROJ) > smart.log
	
# Stamps
$(OUT)map.chg:
	$(Q_STAMP) $(OUT)map.chg
$(OUT)fit.chg:
	$(Q_STAMP) $(OUT)fit.chg
$(OUT)asm.chg:
	$(Q_STAMP) $(OUT)asm.chg
$(OUT)sta.chg:
	$(Q_STAMP) $(OUT)sta.chg
$(OUT)sim.chg:
	$(Q_STAMP) $(OUT)sim.chg

#
# Icarus Verilog
#   icarus: compiles and runs tests using icarus verilog
#      ! Run with TEST defined
#   icarus-compile: only compiles (no simulation) using icarus verilog
#   gtkwave: opens the vvp output in gtkwave
#      ! Run with TEST defined
#
VVP_OPTS=-lxt2
VVP_SIGFILE=$(OUT)$(PROJ)-icarus.vcd
IVER_OPTS=-DSIG_OUT='"$(VVP_SIGFILE)"'

icarus: $(VVP_SIGFILE)
icarus-compile: $(OUT)$(PROJ).vvp
gtkwave: $(VVP_SIGFILE)
	gtkwave $(VVP_SIGFILE)

$(OUT)$(PROJ).vvp: $(OUTDIR) $(SOURCES) $(TEST)
	iverilog $(IVER_OPTS) -o $(OUT)$(PROJ).vvp $(SOURCES) $(TEST)
$(VVP_SIGFILE): $(OUT)$(PROJ).vvp
	vvp $(OUT)$(PROJ).vvp $(VVP_OPTS)


#
# Model-Sim (a little better than quartus)
#   ms-sim: Runs the Model-Sim simulation
#      ! Run with TEST defined
#   ms-time: Runs the Model-Sim timing simulation
#      ! Run with TEST defined
#
MS_LIB_DIR=$(OUT)vsim
MS_LIB_FILE=$(MS_LIB_DIR)/_info
MS_COMPILE_OUT=$(OUT)work

SIM_TIME_ARGS=-t ps +transport_int_delays +transport_path_delays -L $(Q_DEVICE)_ver
Q_EDA_ARGS=--simulation --tool=modelsim_oem --format=verilog

ms-sim: $(MS_COMPILE_OUT)
	$(IN_OUT) \
	$(MS_DIR)vsim $(basename $(TEST))
ms-time: q-map
	$(IN_OUT) \
	$(Q_DIR)quartus_eda $(Q_EDA_ARGS) $(PROJ)
	$(IN_OUT) \
	$(MS_DIR)vsim $(SIM_TIME_ARGS) $(basename $(TEST))

$(MS_COMPILE_OUT): $(MS_LIB_FILE) $(SOURCES) $(TEST)
	$(IN_OUT) \
	$(MS_DIR)vlog $(addprefix ../,$(SOURCES)) $(addprefix ../,$(TEST))

$(MS_LIB_FILE): $(OUTDIR)
	$(MS_DIR)vlib $(MS_LIB_DIR)
