/*
	Lab 3 Top Level Module
	
	Provides an interface for simulation
*/
module lab_3(
	input clock, rdEnable, wrEnable,
	output[15:0] dataOut,
	input[4:0] wrAddr, rdAddr
);
	
	// Instantiate the RAM
	wire[15:0] data;
	ram B1(
		.clock(clock),
		.data(data),
		.rdaddress(rdAddr),
		.rden(rdEnable),
		.wraddress(wrAddr),
		.wren(wrEnable),
		.q(dataOut)
	);
	
	// Instantiate the ALU
	alu #(4) alu1(
		.a(dataOut[12:9]),
		.b(dataOut[8:5]),
		.op(dataOut[15:13]),
		.out(data[3:0])
		//.zflag(data[4])
	);
	
endmodule
