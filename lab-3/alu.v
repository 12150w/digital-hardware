/*
	ALU Module
*/
module alu #(parameter WIDTH = 1) (
	input[2:0] op,
	input signed[WIDTH - 1:0] a, b,
	output reg signed[WIDTH - 1:0] out,
	output zflag
);
	
	// Describe operations
	always @(a, b, op) case(op)
		3'b000: out <= a & b;
		3'b001: out <= a | b;
		3'b010: out <= a + b;
		// 3'b011 not used
		3'b100: out <= a & ~b;
		3'b101: out <= a | ~b;
		3'b110: out <= a - b;
		3'b111: out <= a < b;

		default: out <= 0;
	endcase
	
	// Zero flag
	assign zflag = out == 0;
	
endmodule
