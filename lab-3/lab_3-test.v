/*
	Lab 3 Test Bench
	
	Simulates a couple of the ALU functions
*/
`timescale 1 ns / 1 ns

module lab_3_test;
	parameter TEST_SIZE = 5;
	
	// Control variables
	reg clock, rdEnable, wrEnable;
	wire[15:0] dataOut;
	reg[4:0] rdAddr, wrAddr;
	
	// Instantiate the top level module
	lab_3 device(
		.clock(clock),
		.rdEnable(rdEnable),
		.wrEnable(wrEnable),
		.dataOut(dataOut),
		.rdAddr(rdAddr),
		.wrAddr(wrAddr)
	);
	
	// Initial values
	initial begin
		clock = 1'b0;
		rdEnable = 1'b0;
		wrEnable = 1'b0;
		rdAddr = 5'd0;
		wrAddr = 5'd0;
	end
	
	// Simulate clock input
	always begin
		#5 clock = 1'b1;
		#5 clock = 1'b0;
	end
	
	// Tests
	initial begin : tests
		reg[4:0] testAddr;
		
		for(testAddr = 0; testAddr < TEST_SIZE; testAddr = testAddr + 5'd1) begin
			
			// Set up test read and write address
			rdAddr = testAddr;
			wrAddr = testAddr + 5'd5;
			
			// Read the test data
			rdEnable = 1'b1;
			wrEnable = 1'b0;
			repeat(2) @(posedge clock);
			
			// Write the test output
			rdEnable = 1'b0;
			wrEnable = 1'b1;
			repeat(2) @(posedge clock);
			
			// Read the test output
			rdAddr = wrAddr;
			rdEnable = 1'b1;
			wrEnable = 1'b0;
			repeat(2) @(posedge clock);
			
		end
		
		repeat(2) @(posedge clock);
		$finish;
	end
	
endmodule
